module.exports = {
  tabWidth: 2,
  singleQuote: true,
  printWidth: 200,
  trailingComma: 'none',
  bracketSpacing: false
};

const isDateValid = (...val) => !Number.isNaN(new Date(...val).valueOf());

var date = {
  isDateValid
};

const accAdd = (a, b) => a + b;

var math = {
  accAdd
};

export { date, math };
//# sourceMappingURL=index.js.map

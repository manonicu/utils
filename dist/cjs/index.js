'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const isDateValid = (...val) => !Number.isNaN(new Date(...val).valueOf());

var date = {
  isDateValid
};

const accAdd = (a, b) => a + b;

var math = {
  accAdd
};

exports.date = date;
exports.math = math;
//# sourceMappingURL=index.js.map
